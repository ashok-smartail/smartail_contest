# Import Selenium Web Driver
try:
	from selenium.webdriver import Chrome
	from selenium.webdriver.common.keys import Keys
	from selenium.webdriver.support.ui import Select
	from selenium.webdriver.chrome.options import Options
except Exception as e:
	raise e
# Import time
try:
	from time import sleep
except Exception as e:
	raise e
import csv
import add_user
import send_email
import config
import os
import threading
from random import randint


# NEW STUDENT VARIABLES
NUMBER_OF_STUDENTS = 0
INITIAL_ROLL_NO = 1000000
ALL_STUDENTS = list()
log = open("log.txt", "a")

def random_with_N_digits(n=10):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return str(randint(range_start, range_end))


class Student:
	def __init__(
		self, 
		fname="ASHOK KUMAR",
		lname="K",
		school="Smartail",
		class_="V", 
		section="A",
		gender="Male",
		email="a@a.a",	 
		mobile="9876543210",
		city="Chennai"):
		global NUMBER_OF_STUDENTS
		self.fname = fname
		self.lname = lname
		self.school = school
		self.class_ = class_
		self.section = section
		self.gender = gender
		self.email = email
		self.mobile = mobile
		NUMBER_OF_STUDENTS += 1
		self.roll_no = INITIAL_ROLL_NO + NUMBER_OF_STUDENTS
		self.login_id = "".join("{}{}".format(fname, lname).split())
		self.password = random_with_N_digits()
		self.city = city

	def __str__(self):
		return "Firstname: {}, Lastname: {}, Roll_No: {}".format(
			self.fname, 
			self.lname,
			self.roll_no
		)


def hit_login_page():
	global log
	try:
		driver.get("https://discord.com/login")
		driver.maximize_window()
	except Exception as e:
		log.write("\n[Exception Info] : {}".format(e))
		sleep(5)
		hit_login_page()
	else:
		log.write("\n[Success Info] : {}".format("Successfully Opened Login Page"))
		sleep(5)


def script_login():
	global log
	try:
		driver.execute_script(
			"""
			function login(token) {
			setInterval(() => {
			document.body.appendChild(document.createElement `iframe`).contentWindow.localStorage.token = `"${token}"`
			}, 50);
			setTimeout(() => {
			location.reload();
			}, 2500);
			}; 
			login("NzY3OTYwMjA0NzEzMTMyMDgy.X45g-Q.lgOFMRwa2O-YA62HKR_-fNLvIss")
			"""
		)
	except Exception as e:
		log.write("\n[Exception Info] : {}".format(e))
		sleep(5)
		script_login()
	else:
		log.write("\n[Success Info] : {}".format("Successfully Logged in with script"))
		sleep(5)


def choose_server():
	global log
	repeat = 0
	try:
		driver.find_element_by_css_selector('a[href="/channels/{}"]'.format(server_id)).click()
	except Exception as e:
		repeat += 1
		if repeat == 3:
			repeat = 0
			driver.refresh()
		log.write("\n[Exception Info] : {}".format(e))
		sleep(2)
		choose_server()
	else:
		log.write("\n[Success Info] : {}".format("Successfully Choosed Server......"))
		sleep(5)


def save_data_to_csv(all_students):
	with open("students.csv", "a") as data:
		writer = csv.writer(data)
		for student in all_students:
			stud = list([
				student.roll_no,
				student.fname,
				student.lname,
				student.school,
				student.class_,
				student.section,
				student.email,
				student.mobile,
				student.city,
				student.login_id,
				student.password
			])
			writer.writerow(stud)


def add_students_to_deepgrade(all_students):
	for student in all_students:
		if student.email !=  "ashokkumar.datascience@gmail.com":
			add_user.hit_deepgrade_api(student)
			sleep(10)


def send_mail_to(all_students):
	recievers = []
	messages = []
	for student in all_students:
		if student.email !=  "ashokkumar.datascience@gmail.com":
			recievers.append(student.email)
			message = (student.login_id, student.password)
			messages.append(message)
	send_email.send(
		config.SENDER_EMAIL,
		config.SENDER_PASS,
		recievers,
		"Smartail Student",
		messages=messages
	)


######################################################################################################################
def get_all_messages():
	global log
	messages = driver.find_elements_by_css_selector('div[class="grid-1nZz7S"]')
	messages = list(messages)[NUMBER_OF_STUDENTS:]
	log.write("\n[Info] : NUMBER_OF_STUDENTS : {}".format(NUMBER_OF_STUDENTS))
	log.write("\nNUMBER_OF_MESSAGES : {}".format(len(messages)))
	if len(messages) == 0:
		log.write("\nWaiting for new message")
	else:
		for message in messages:
			msg = message.get_attribute('innerText').splitlines()
			ALL_STUDENTS.append(Student(
				fname=msg[2],
				lname=msg[4],
				gender=msg[6], 
				email=msg[8], 
				mobile=msg[10], 
				class_=msg[12],
				school=msg[14]
			))
		save_data_to_csv(ALL_STUDENTS)
		log.write("\nData Saved To CSV")
		# add_students_to_deepgrade(ALL_STUDENTS)
		log.write("\nStudents added to smartail contest")
		# send_mail_to(ALL_STUDENTS)
		log.write("\nMail sent to Students")
		del messages
		sleep(20)
		driver.get("https://discord.com/channels/{}".format(server_id))
		sleep(10)
########################################################################################################################


def listen_discord(server_id_):
	global driver, server_id, log
	options = Options()
	options.add_argument("--headless")
	options.add_argument("--incognito")
	driver_path = str(os.path.abspath('.') + "/drivers/chromedriver")
	driver = Chrome(driver_path, options=options)
	# driver = Chrome(driver_path)
	server_id = server_id_
	
	log.write("\nLogin to Discord.....")
	hit_login_page()
	script_login()
	log.write("\nLogin Completed")
	sleep(10)
	choose_server()
	while True:
		get_all_messages()
		sleep(100)
	log.close()


if __name__ == '__main__':
	listen_discord(config.SERVER_ID)