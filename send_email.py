import smtplib
import time

from email.message import EmailMessage
import imghdr

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

def send(
	from_address, 
	from_address_pwd, 
	to_addresses,
	subject,
	messages=None,
	flag=True,
	notification_content=None):	

	mailserver = smtplib.SMTP('smtp.gmail.com',587)
	# identify ourselves to smtp gmail client
	mailserver.ehlo()
	# secure our email with tls encryption
	mailserver.starttls()
	# re-identify ourselves as an encrypted connection
	mailserver.ehlo()
	log = open("log.txt", "a")
	try:
		mailserver.login(from_address, from_address_pwd)
	except Exception as e:
		log.write("Error Occured on Login")
	else:
		log.write("Login Success")
	for i in range(len(to_addresses)):
		try:
			msg = MIMEMultipart('related')
			msg['Subject'] = subject
			msg['From'] = from_address
			msg['To'] = to_addresses[i]
			# msg.set_content(messages[i])
			msg.preamble = 'This is a multi-part message in MIME format.'

			msgAlternative = MIMEMultipart('alternative')
			msg.attach(msgAlternative)

			msgText = MIMEText('This is the alternative plain text message.')
			msgAlternative.attach(msgText)

			# We reference the image in the IMG SRC attribute by the ID we give it below
			if flag:
				content = """
				<div dir="ltr">
	   <div style="text-align:center"><font size="6" color="#0b5394"><b>Hey Kiddo !!</b></font></div>
	   <div style="text-align:center"><span style="font-size:large;color:rgb(11,83,148)"><br></span></div>
	   <div style="text-align:center"><span style="font-size:large"><font color="#000000">Welcome to </font><b><font color="#cc0000">DeepGrade</font></b><font color="#000000"> Contest</font></span><br></div>
	   <div style="text-align:center">
	      <div><img src="cid:image1" alt="Smartail Logo New.png" width="103" height="60" style="margin-right:0px" data-image-whitelisted="" class="CToWUd"><br></div>
	   </div>
	   <div style="text-align:center"><span style="font-size:xx-large">Your Registration Confirmed</span><br></div>
	   <div style="text-align:center"><br></div>
	   <div style="text-align:center"><font size="4"><i> &nbsp;Login ID: {}</i></font></div>
	   <div style="text-align:center"><font size="4"><i> &nbsp;Password: {}</i></font></div>
	   <div style="text-align:center"><br></div>
	   <div style="text-align:center"><br></div>
	   <div style="text-align:center"><b><i> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Step 1: Download DeepGrade App&nbsp;</i></b></div>
	   <div style="text-align:center"><b><i> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Step 2: Click on Assignments in menu</i></b></div>
	   <div style="text-align:center"><b><i> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Step 3: Submit your Essay&nbsp;</i></b></div>
	   <div style="text-align:center"><br></div>
	   <div style="text-align:center">
	      <div id="m_-8140206543559550143gmail-:va" style="font-size:0.875rem;direction:ltr;margin:8px 0px 0px;padding:0px;font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;text-align:start">
	         <div id="m_-8140206543559550143gmail-:vb" style="overflow:hidden;font-variant-numeric:normal;font-variant-east-asian:normal;font-stretch:normal;font-size:small;line-height:1.5;font-family:Arial,Helvetica,sans-serif">
	            <div dir="ltr">
	               <div dir="ltr">
	                  <div dir="ltr">
	                     <div dir="ltr">
	                        <div dir="ltr">
	                           <div dir="ltr">
	                              <div style="text-align:center">
	                                 <div>&nbsp; &nbsp;&nbsp;<a href="https://twitter.com/SMARTAILABS" style="font-size:x-small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://twitter.com/SMARTAILABS&amp;source=gmail&amp;ust=1603878487475000&amp;usg=AFQjCNEJjs3yjvW7pc66E3jWGz_-gAqdvQ"><img src="cid:image2" alt="twitter.png" width="23" height="23" data-image-whitelisted="" class="CToWUd"></a><span style="font-size:x-small">&nbsp; &nbsp;</span><a href="http://www.linkedin.com/company/30967367" style="font-size:x-small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.linkedin.com/company/30967367&amp;source=gmail&amp;ust=1603878487475000&amp;usg=AFQjCNEfEPMLz1Bcvo0-M79ZNwMgbebW6g"><img src="cid:image3" alt="linkedin.png" width="23" height="23" data-image-whitelisted="" class="CToWUd"></a><span style="font-size:x-small">&nbsp; &nbsp;</span><a href="https://www.facebook.com/Smartail-110858173859975" style="font-size:x-small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/Smartail-110858173859975&amp;source=gmail&amp;ust=1603878487475000&amp;usg=AFQjCNF7JGv29RfIJnr08q6aEImwMLETLw"><img src="cid:image4" alt="facebook.png" width="23" height="23" data-image-whitelisted="" class="CToWUd"></a><span style="font-size:x-small">&nbsp;&nbsp;</span><a href="https://www.instagram.com/smartail_/" style="font-size:x-small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.instagram.com/smartail_/&amp;source=gmail&amp;ust=1603878487475000&amp;usg=AFQjCNF51Dk6myegxOld2aB4tRmVzf2VhA"><img src="cid:image5" alt="instagram-sketched.png" width="23" height="23" data-image-whitelisted="" class="CToWUd"></a><span style="font-size:x-small">&nbsp;&nbsp;</span><a href="http://smartail.ai/" style="font-size:x-small" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://smartail.ai/&amp;source=gmail&amp;ust=1603878487475000&amp;usg=AFQjCNEezfYhM837vVJeQRnVU0E0T1nQbQ"><img src="cid:image6" alt="internet.png" width="20" height="20" data-image-whitelisted="" class="CToWUd"></a><br></div>
	                                 <div>
	                                    <div>
	                                       <div><i style="font-family:HelveticaNeue,sans-serif"><br></i></div>
	                                       <div><i style="font-family:HelveticaNeue,sans-serif">Need any help?</i><br></div>
			                               <div>
	                                          <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt">Write to us at&nbsp;<a href="mailto:reachus@smartail.ai" target="_blank">reachus@smartail.ai</a>&nbsp;or call us.<span style="font-family:Calibri,sans-serif;font-size:11pt">+91 9019404915.</span>&nbsp;&nbsp;<br></p>
	                                       </div>
	                                    </div>
	                                 </div>
	                              </div>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	               </div>
	            </div>
	            <div class="yj6qo"></div>
	            <div class="adL"></div>
	            <div class="adL"></div>
	         </div>
	      </div>
	      <div id="m_-8140206543559550143gmail-:1fo" style="margin:15px 0px;clear:both;font-size:0.875rem;font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;text-align:start" class="adL"><br></div>
	   </div>
	   <div style="text-align:center" class="adL"><br></div>
	   <div style="text-align:center" class="adL"><br></div>
	</div>
				""".format(messages[i][0], messages[i][1])
				msgText = MIMEText('{}'.format(content), 'html')
				msgAlternative.attach(msgText)

				# Smartail
				fp = open('smartail.png', 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image1>')
				msg.attach(msgImage)

				# Twitter
				fp = open('twitter.png', 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image2>')
				msg.attach(msgImage)

				# LinkedIn
				fp = open('linkedin.png', 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image3>')
				msg.attach(msgImage)

				# Facebook
				fp = open('facebook.png', 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image4>')
				msg.attach(msgImage)

				# Instagram
				fp = open('instagram.png', 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image5>')
				msg.attach(msgImage)

				# WWW
				fp = open('www.png', 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image6>')
				msg.attach(msgImage)
			else:
				content = "Unable to add the following student : {}".format(notification_content)
				msgText = MIMEText('{}'.format(content), 'html')
				msgAlternative.attach(msgText)
			
			mailserver.send_message(msg)
			del msg
		except Exception as e:
			print(str(e))
			print("Error Occured sending mail to {}".format(to_addresses[i]))
	mailserver.quit()


if __name__ == '__main__':
	start = time.time()
	from_ = "ashokkumark.citeee2017@gmail.com"
	from_pwd = "1085akdk"
	to_ = ["ashokkumar.smartail@gmail.com"]
	subject = "Mail with Attachment using python"
	body = [("asdfs", "asdfghgfdsa")]
	send(from_, from_pwd, to_, subject, body)
	# send(from_, from_pwd, to_, subject, flag=False, notification_content="{}".format("kusdgcvygbdiabsgdvcbSdcvdvcvaikscvusavscdus"))
	end = time.time()
	print("Time Taken : {}".format(end - start))