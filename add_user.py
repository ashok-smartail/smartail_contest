# Import time
try:
	from time import sleep
except Exception as e:
	raise e
import config
import requests
import send_email
import json

deepgrade_api_structure = json.load(open("deepgrade_api_structure.json", "r"))

def assign_new_student(student):
	global deepgrade_api_structure
	deepgrade_api_structure["body"]["firstName"] = student.fname
	deepgrade_api_structure["body"]["lastName"] = student.lname
	deepgrade_api_structure["body"]["standard"] = student.class_
	deepgrade_api_structure["body"]["section"] = student.section
	deepgrade_api_structure["body"]["rollNumber"] = student.roll_no
	deepgrade_api_structure["body"]["sex"] = student.gender
	deepgrade_api_structure["body"]["emailId"] = student.email
	deepgrade_api_structure["body"]["loginId"] = student.login_id
	deepgrade_api_structure["body"]["Password"] = student.password
	deepgrade_api_structure["body"]["confirmPassword"] = student.password
	deepgrade_api_structure["headers"]["Password"] = student.password
	
	return deepgrade_api_structure


def hit_deepgrade_api(student):
	structure = assign_new_student(student)
	response = requests.post(
		url=structure["api_url"], 
		headers=structure["headers"], 
		json=structure["body"], 
		timeout=2.50
	)
	with open("deepgrade_students.txt", "a") as log:
		if response.status_code == 201:
			log.write("Student : {} added Successfully\
			 with response code : {}".format(student, response.status_code))
		else:
			log.write("Unable to Add Student. {}".format(student))
			to_addresses = [
				"ashokkumar.smartail@gmail.com", 
				"dewey@smartail.ai", 
				"radhika@smartail.ai",
			]
			send_email.send(
				config.SENDER_EMAIL,
				config.SENDER_PASS,
				to_addresses,
				subject="Unable to add below student to smartail contest.",
				flag=False,
				notification_content="{}".format(structure)
			)
		hit_wait_time = 60
		log.write("Waiting for next API Hit : {}s".format(hit_wait_time))
		sleep(hit_wait_time)

# class Sample:
# 	def __init__(self):
# 		pass

# if __name__ == "__main__":
# 	a = Sample()
# 	a.fname = "Radhika"
# 	a.lname = "Udhayakumar"
# 	a.class_ = "VIII"
# 	a.section = "A"
# 	a.roll_no = "1234565432"
# 	a.gender = "Female"
# 	a.email = "radhika@dc.in"
# 	a.login_id = "radhika123@dc.in"
# 	a.password = "deepgrade"
# 	hit_deepgrade_api(a)