import discord_user
import threading
import time
import config

def main():
	daemon = threading.Thread(
		target=discord_user.listen_discord(config.SERVER_ID), 
		name="Discord-Thread", 
		daemon=True
	)
	daemon.start()


if __name__ == '__main__':
	try:
		main()
	except Exception as e:
		print(e)
		with open("log.txt", "a") as log:
			log.write("\n\n\n\n\n Running Main Again \n\n\n\n\n")